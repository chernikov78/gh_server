#!/usr/bin/env python3

import time, serial, threading,os, socket, pickle, sys
import sqlalchemy
import gh_conf
from datetime import datetime
from collections import deque
from  daemon import Daemon
import os

class MyDaemon(Daemon):
	def __init__(self, pidfile, socket_param, device):
		self.pidfile = pidfile
		self.socket_param = socket_param
		self.device = device
		self.ser_out_queue = deque()
		self.ser_in_queue = deque()
		self.soc_out_queue = deque()
		self.event_soc_out = threading.Event()
		self.event_ser_in = threading.Event()
		self.define_delay = 3
		self.max_packages = 2

	def run(self):
		# if os.path.exists(self.filepath):
		# 	os.remove(self.filepath)
		serial_work = threading.Thread(name='serial_work', target=runmain,args=(self,))
		serial_work.start()
		sqlwriter = threading.Thread(name='sqlwriter',target=run_sqlwriter,args=(self,))
		sqlwriter.start()
		socketworker = threading.Thread(name='socketworker',target=run_socketworker,args=(self,))
		socketworker.start()
	
	def after_stop(self):
		if os.path.exists(self.socket_param):
			os.remove(self.socket_param)


def runmain(self):
	ser = serial.Serial(self.device,9600)
	ser.bytesize = serial.SEVENBITS
	ser.parity = serial.PARITY_EVEN
	ser.stopbits = serial.STOPBITS_ONE
	ser.timout = 0
	ser.xonxof = False
	ser.rtscts = False
	ser.dsrdtr = False
	try:
		ser.close()
		ser.open()
	except ValueError:
		sys.exit(2)
	if ser.is_open:
		ser.reset_input_buffer()
		ser.reset_output_buffer()
		counter_packages = 0
		new_resp = bytearray()
		count_pack_out = 0
		example_pack = b''
		start_time = 0
		while 1:
			if (ser.in_waiting==0):
				#here I have to check the outgoing queue
				while len(self.ser_out_queue):
					wdc = b'000'
					out_pack = self.ser_out_queue.pop()
					count_pack_out = out_pack['pack_qt']
					example_pack = out_pack['pack_waiting']
					wdc += out_pack['package'][1].to_bytes(1, 'big')
					wdc += out_pack['package'][2].to_bytes(1, 'big')
					wdc += out_pack['package'][3].to_bytes(1, 'big')
					ser.write(out_pack['package'])
					start_time = time.time()
					# print ('To MCU')
				time.sleep(0.1)
			response = b''
			if start_time > 0:
				if (time.time() - start_time) > self.define_delay:
					start_time = 0
					self.soc_out_queue.appendleft(b'~')
					self.event_soc_out.set()
					while self.event_soc_out.is_set():
						pass
				else:
					pass
			else:
				pass
			while ser.in_waiting:
				response += ser.read(1)
			if response!=b'':
				for val in response:
					if val ==0x21:
						new_resp = bytearray()
					else:
						if val==0x0d:
							d=dict(dt=datetime.now(),package=new_resp)
							#here I have to put  d into the queue
							self.ser_in_queue.appendleft(d)
							counter_packages +=1
							# print('From MCU')
							if count_pack_out!=0:
								if wdc in new_resp:
									count_pack_out-=1
									self.soc_out_queue.appendleft(new_resp)
								else:
									pass
								if example_pack in new_resp or count_pack_out==0:
									if len(self.soc_out_queue)>0:
										self.event_soc_out.set()
									start_time = 0
									while self.event_soc_out.is_set():
										pass
							if counter_packages == self.max_packages:
								counter_packages = 0
								if len(self.ser_in_queue)>0:
									self.event_ser_in.set()
								while self.event_ser_in.is_set():
									pass
							else:
								pass
						else:
							new_resp += val.to_bytes(1,'little')

def run_sqlwriter(self):
	while 1:
		self.event_ser_in.wait()
		for _ in range(0,len(self.ser_in_queue)):
			log = open('/var/tmp/gh/mysql.log', 'a')
			el = self.ser_in_queue.pop()
			log.write ('\n\n{}\nget data from queue {}'.format(datetime.now(), str(el)))
			pack_ = el['package'].decode('utf-8')
			from_=pack_[3:6]
			to_= pack_[0:3]
			if pack_[6:7]=='l':
				data_= pack_[7:]
				lg= '1'
			else:
				data_= pack_[6:]
				lg= '0'
			log.write('\ndecode data and start writing to db {}'.format({'dt':el['dt'], 'package':pack_, 'from_':from_, 'to_':to_, 'data_':data_, 'log_data':lg}))
			engine = sqlalchemy.create_engine(gh_conf.MySQL)
			conn = engine.connect()
			sql = """INSERT INTO LOG(dt, package, from_, to_, data_, log_data) VALUE('%(dt)s', '%(package)s', '%(from_)s', '%(to_)s', '%(data_)s', '%(log_data)s')"""%{'dt':el['dt'], 'package':pack_, 'from_':from_, 'to_':to_, 'data_':data_, 'log_data':lg}
			_ =conn.execute(sql)
			_.close()
			conn.close()
			log.write('\n connection closed')
			engine.dispose()
			log.write('\n engine destroyed')
			log.close()
		self.event_ser_in.clear()

def run_socketworker(self):
	with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as ss:
		ss.bind(self.socket_param)
		ss.listen(5)
		while True:
			conn, _ = ss.accept()
			# log = open('/var/tmp/gh/socket.log', 'a')
			# log.write ('\n\n{}\nrecieve data from socket {}'.format(datetime.now(), str(_)))
			with conn:
				try:
					data = conn.recv(128)
					if data!=b'':
						d={}
						d=pickle.loads(data)
						self.ser_out_queue.appendleft(d)
						# log.write('\nadd data to queue to serial ' + str(d))
						self.event_soc_out.wait(5)
						# log.write('\nreceive data from serial or timeout')
						for _ in range(0,len(self.soc_out_queue)):
							el = self.soc_out_queue.pop()
							conn.send(el)
							# log.write('\nget data from serial and pu it to socket ' + str(el))
				finally:
					self.event_soc_out.clear()
			# log.close()
		del ss


if __name__=="__main__":
	myDaemon = MyDaemon(gh_conf.PIDFile, gh_conf.Socket, gh_conf.Device)
	if len(sys.argv) ==2:
		if 'start'==sys.argv[1]:
			myDaemon.start()
		elif 'stop'==sys.argv[1]:
			myDaemon.stop()
		elif 'restart'==sys.argv[1]:
			myDaemon.restart()
		else:
			print("Unknown command")
			sys.exit(2)
		sys.exit(0)
	else:
		print("usage: %s satrt|stop|restart" % sys.argv[0])
		sys.exit(2)
	
