#!/usr/bin/env python3

import time, serial, MySQLdb, threading,os, socket, pickle
from datetime import datetime
from collections import deque
from pdem import pdem

MAX_PACKAGES_SER = 100
FILEPATH = "/tmp/python_unix_socket"
DEVICE = "/dev/ttyUSB0"
DEFINE_DELAY = 3

class Ser(threading.Thread):
	def __init__(self, device):
		super(Ser,self).__init__()
		self.__device = device

	def run(self):
		ser = serial.Serial(self.__device,9600)
		ser.bytesize = serial.SEVENBITS
		ser.parity = serial.PARITY_EVEN
		ser.stopbits = serial.STOPBITS_ONE
		ser.timout = 0
		ser.xonxof = False
		ser.rtscts = False
		ser.dsrdtr = False
		try:
			ser.close()
			ser.open()
		except ValueError:
			exit()
		if ser.is_open:
			ser.reset_input_buffer()
			ser.reset_output_buffer()
			counter_packages = 0;
			new_resp = bytearray()
			count_pack_out = 0
			example_pack = b''
			start_time = 0
			while 1:
				if ser.in_waiting==0:
					#here I have to check the outgoing queue
					while len(ser_out_queue):
						wdc = b'000'
						out_pack = ser_out_queue.pop()
						count_pack_out = out_pack['pack_qt']
						example_pack = out_pack['pack_waiting']
						wdc += out_pack['package'][1].to_bytes(1, 'big')
						wdc += out_pack['package'][2].to_bytes(1, 'big')
						wdc += out_pack['package'][3].to_bytes(1, 'big')
						ser.write(out_pack['package'])
						start_time = time.time()
						print ('To MCU')
				response = b''
				if start_time > 0:
					#import pdb
					#pdb.set_trace()
					if (time.time() - start_time) > DEFINE_DELAY:
						start_time = 0
						soc_out_queue.appendleft(b'~')
						event_soc_out.set()
						while event_soc_out.is_set():
							pass
					else:
						pass
				else:
					pass
				while ser.in_waiting:
					response += ser.read(1)
				if response!=b'':
					for val in response:
						if val ==0x21:
							new_resp = bytearray()
						else:
							if val==0x0d:
								d=dict(dt=datetime.now(),package=new_resp)
								#here I have to put  d into the queue
								ser_in_queue.appendleft(d)
								counter_packages +=1
								print('From MCU')
								#import pdb
								#pdb.set_trace()
								if count_pack_out!=0:
									if wdc in new_resp:
										count_pack_out-=1
										soc_out_queue.appendleft(new_resp)
									else:
										pass
									if example_pack in new_resp or count_pack_out==0:
										if len(soc_out_queue)>0:
											event_soc_out.set()
										start_time = 0
										while event_soc_out.is_set():
											pass
								if counter_packages == MAX_PACKAGES_SER:
									counter_packages = 0
									if len(ser_in_queue)>0:
										event_ser_in.set()
									while event_ser_in.is_set():
										pass
								else:
									pass
							else:
								new_resp += val.to_bytes(1,'little')

class SQLWriter(threading.Thread):
	def __init__(self):
		super(SQLWriter,self).__init__()

	def run(self):
		while 1:
			event_ser_in.wait()
			db = MySQLdb.connect(host='localhost', user='root', passwd='mhvter', db='g_house', charset='utf8')
			cursor = db.cursor()
			for i in range(0,len(ser_in_queue)):
				el = ser_in_queue.pop()
				sql = """INSERT INTO LOG(dt,package) VALUE('%(dt)s', '%(package)s')"""%{'dt':el['dt'], 'package':el['package'].decode('utf-8')}
				cursor.execute(sql)
			db.commit()
			db.close()
			event_ser_in.clear()

class SocketWorker(threading.Thread):
	def __init__(self):
		super(SocketWorker,self).__init__()

	def run(self):
		ss = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
		ss.bind(FILEPATH)
		ss.listen(1)
		while 1:
			conn, addr = ss.accept()
			try:
				data = conn.recv(100)
				if data!=b'':
					d={}
					#import pdb
					#pdb.set_trace()
					d=pickle.loads(data)
					ser_out_queue.appendleft(d)
					event_soc_out.wait()
					for i in range(0,len(soc_out_queue)):
						el = soc_out_queue.pop()
						conn.send(el)
			finally:
				conn.close()
				event_soc_out.clear()

if os.path.exists(FILEPATH):
	os.remove(FILEPATH)
ser_out_queue = deque()
ser_in_queue = deque()
soc_out_queue = deque()
event_soc_out = threading.Event()
event_ser_in = threading.Event()
serial_work = Ser(DEVICE)
serial_work.start()
sqlwriter = SQLWriter()
sqlwriter.start()
socketworker = SocketWorker()
socketworker.start()
#d = dict(package=b'!0010001\r',pack_qt=1,pack_wiating=b'')
#ser_out_queue.appendleft(d)
